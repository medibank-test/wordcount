<?php

$dir = "DIRECTORY"; //full directory path
$words = ["word1", "word2", "word3"]; // words to be searched
$files = array_diff(scandir($dir), array('..', '.')); //get all the files and folders in directory
$final_words = [];

foreach ($files as $filename) {
    $filePath = $dir . DIRECTORY_SEPARATOR . $filename;
    if (is_file($filePath)) { //checking if it is a file
        $file = file_get_contents($filePath, true); //get the content of file
        $final_words = array_merge($final_words, str_word_count($file, 1));
    }
}
$final_words = array_intersect_key(array_count_values($final_words), array_flip($words)); //counting the words and selecting only based on the words
arsort($final_words); //sorting the array based on values


//print the list words and occurrences
foreach ($final_words as $word => $occurrence ) {
    echo "$word $occurrence\n";
}
